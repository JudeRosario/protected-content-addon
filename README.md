# README #

This set of files can upgrade the Protected Content plug-in to support Invite Codes, either replace the existing contents of your `protected-content` folder with the contents of this repo or clone and install it as a separate plug-in.

### What is this repository for? ###

### Quick summary

  - Generate Invite codes, and allow manual addition/edits of invite codes
  - Option to remove codes automatically once used by setting maximum uses per code
  - Restrict to a specific subscription, so they can only sign up to site on that specific subscription
  - Option to only allow signup to site if they have a code
  - Form can be configured to display on signup form and login page


### How do I get set up? ###


  * Install WordPress / Wordpress + MS if not done so already 

  * Get a membership from [here](http://premium.wpmudev.org) 
  
  * [Download and install the protected content plug-in](https://premium.wpmudev.org/project/protected-content/) 
  
  * Clone this repo and overwrite the plugin files in merge mode
  
  * Setup the base plug in as shown [here](https://premium.wpmudev.org/project/protected-content/?utm_expid=3606929-14.ck9fZkaDQZ-_8txqL-xJbQ.0&utm_referrer=https%3A%2F%2Fwww.google.co.in%2F#usage)

  * Enable the Invite Code addon from the addons screen

    ![Enable](http://s18.postimg.org/fvdlql8vt/screen_enable.png)

  * Add a new coupon or edit using this screen 
    
    ![Edit](http://s21.postimg.org/400d8j56f/screen_edit.png)   

  * You can view / edit available coupons using options shown in this screen 

    ![View](http://s13.postimg.org/mrzqo69lz/screen_view.png)



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Maintainer

judesrosario89@gmail.com
